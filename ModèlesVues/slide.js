var currentSlide = 1;
var nbSlides = 3;
var timer;

function looper(){
	update();
	currentSlide++;
	if(currentSlide > nbSlides){
		currentSlide = 1;
	}
}

function update(){
	for(i=1; i<=nbSlides; i++){
		hide(i);
	};
	show(currentSlide);
}

function hide(index){
	div=document.getElementById('slide'+index);
	div.setAttribute('class','livreCelebre disabled');
}

function show(index){
	div=document.getElementById('slide'+index);
	div.setAttribute('class','livreCelebre enabled');	
}

function droit(){
	currentSlide++;
	if(currentSlide > nbSlides){
		currentSlide = 1;
	}
	console.log(currentSlide);
	update();
	pause();
}

function gauche(){
	currentSlide--;
	if(currentSlide < 1){
		currentSlide = nbSlides;
	}
	console.log(currentSlide);
	update();
	pause();
}

function pause(){
	window.clearInterval(timer);
	setInterval(function(){},3000);
	timer = setInterval(looper,3000);
}

timer = setInterval(looper,3000);