<!DOCTYPE html>
<html>
<head>
	<!-- Balise méta : définition des propriétés de la page, référencement, ... -->
	<meta charset="utf-8">
	<!-- Inclusion des scripts CSS -->
	<link rel="stylesheet" type="text/css" href="webroot/css/theme.css">
	<link rel="stylesheet" type="text/css" href="webroot/css/style.css">
	<link rel="stylesheet" type="text/css" href="webroot/css/accueil.css">
	<title><?php echo $this->_title; ?></title>
</head>
<body>
<!-- Container -->
	<div>

	<!-- Menu -->  
	<?php  	echo $content_menu;  ?>

	<!-- Entete --> 
	<div>
		<img src="/images/adele.png" alt="loading..." style="width:700px;height:90px;" />
	</div>

	<!-- Corps --> 
	<?php  	echo $content_vue; ?>
	
	<!-- Footer : Pied de page --> 
	<div>
	Site créé par les membres du SDE
	</div>
	</div> <!-- Fin Container -->

	<!-- Autres scripts -->
	<script type="javascript"></script>
</body>
</html>