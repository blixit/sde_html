<?php
	class User extends Modele{
		private $id = 0;
		private $nom = "";
		private $prenom = "";
		private $login = "";
		private $pass = "";
		private $inscription = NULL;


		function __construct(){
			//var_dump(self::$db);
			$this->_default_tables = 'user';

		}

		/// GETTERS ET SETTERS
		public function gid(){			
			return $this->id;	
		}
		public function sid($value){			
			$this->id = $value;		
		}
		public function gnom(){			
			return $this->nom;	
		}
		public function snom($value){			
			$this->nom = $value;		
		}
		public function gprenom(){			
			return $this->prenom;	
		}
		public function sprenom($value){			
			$this->prenom = $value;		
		}
		public function glogin(){			
			return $this->login;	
		}
		public function slogin($value){			
			$this->login = $value;		
		}
		public function gpass(){			
			return $this->pass;	
		}
		public function spass($value){			
			$this->pass = $value;		
		}
		public function ginscription(){			
			return $this->inscription;	
		}
		public function sinscription($value){			
			$this->inscription = $value;		
		}



		public function add(){ 
			$erreur;
			$idreq;
			$options = array('values' => 
				Functions::squote($this->gid()).",".
				Functions::squote($this->gnom()).",".
				Functions::squote($this->gprenom()).",".
				Functions::squote($this->glogin()).",".
				Functions::squote($this->gpass()).",".
				Functions::squote($this->ginscription())
			);

			$this->create($options,$erreur, $idreq);
			var_dump($options);
		}

		public function get($options = array()){ 
			//on écrase la valeur de options['tables'] si elle est rentrée dans le controller
			//$options["tables"] = $this->_default_tables;
			
			$resultat = $this->read($options); 
			
			//Mapping
			$users = array();
			foreach ($resultat as $key => $value) {
				$user = new User();
				$user->sid($value['id']);
				$user->snom($value['nom']);
				$user->sprenom($value['prenom']);
				$user->slogin($value['login']);
				$user->spass($value['pass']);
				$user->sinscription($value['inscription']);

				array_push($users, $user);
			}
			return $users;
		}
				// En vez de de crear una tabla con diferentes lineas de utilisaters, creas objetos con cada utilisateur

		public function up($options = array()){ 
			$this->update(array());
		}

		public function del($options = array()){ 
			$this->delete(array());
		}
	}
