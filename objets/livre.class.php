<?php
	class Livre extends Modele{
		private $_id = 0; //identifiant
		private $_titre =""; // titre du livre
		private $_auteur = ""; // auteur du livre
		private $_annee = ""; // annee de parution
		private $_id_genre = 0; // identifiant du genre du livre
		private $_id_categorie = 0; // identifiant de la categorie du livre

		function __construct(){
			$this->_default_tables = 'livre';
		}

		// GETTERS & SETTERS
		
		public function gid(){
			return $this->_id;
		}

		public function sid($value){
			$this->_id = $value;
		}

		public function gtitre(){
			return $this->_titre;
		}

		public function stitre($value){
			$this->_titre = $value;
		}

		public function gauteur(){
			return $this->_auteur;
		}

		public function sauteur($value){
			$this->_auteur = $value;
		}

		public function gannee(){
			return $this->_annee;
		}

		public function sannee($value){
			$this->_annee = $value;
		}

		public function gidgenre(){
			return $this->_id_genre;
		}

		public function sidgenre($value){
			$this->_id_genre = $value;
		}

		public function gidcategorie(){
			return $this->_id_categorie;
		}

		public function sidcategorie($value){
			$this->_id_categorie = $value;
		}

		// CRUD FONCTIONS

		public function add(){
			$erreur;
			$idreq;
			$options = array('values' =>
				Functions::squote($this->gid()).",".
				Functions::squote($this->gtitre()).",".
				Functions::squote($this->gauteur()).",".
				Functions::squote($this->gannee()).",".
				Functions::squote($this->gidgenre()).",".
				Functions::squote($this->gidcategorie())
			);

			$this->create($options,$erreur,$idreq);
		}

		public function get($options = array()){
			
			$resultat = $this->read($options);

			$livres = array();
			foreach ($resultat as $key => $value){
				$livre = new Livre();
				$livre->sid($value['id']);
				$livre->stitre($value['titre']);
				$livre->sauteur($value['auteur']);
				$livre->sannee($value['annee']);
				$livre->sidgenre($value['id_genre']);
				$livre->sidcategorie($value['id_categorie']);

				array_push($livres, $livre);
			}
			return $livres;
		}

		public function up($options = array()){
			$this->update($options);
		}

		public function del($options = array()){
			$this->delete($options);
		}

	}