<?php
	class reservation extends Modele{
		private $_id = 0; //identifiant
		private $_date_debut = ''; //date de reservation
		private $_date_fin = ''; //date de retour
		private $id_livre = 0; //identifiant du livre
		private $id_user = 0; //identifiant de l'utilisateur

		function __construct(){
			$this->_default_tables = 'reservation';
		}

		// GETTERS & SETTERS 
		//Para proteger las clases y que nadie pueda tocar los campos. 

		public function gid(){
			return $this->_id;
		}

		public function sid($value){
			$this->_id = $value;
		}

		public function gdatedebut(){
			return $this->_date_debut;
		}

		public function sdatedebut($value){
			$this->_date_debut = $value;
		}

		public function gdatefin(){
			return $this->_date_fin;
		}

		public function sdatefin($value){
			$this->_date_fin = $value;
		}

		public function gidlivre(){
			return $this->_id_livre;
		}

		public function sidlivre($value){
			$this->_id_livre = $value;
		}

		public function giduser(){
			return $this->_id_user;
		}

		public function siduser($value){
			$this->_id_user = $value;
		}

		// CRUD FONCTIONS
		// -----

		public function add(&$erreur){ 
			$idreq;
			$options = array('values' =>
				Functions::squote($this->gid()).",".
				Functions::squote($this->gdatedebut()).",".
				Functions::squote($this->gdatefin()).",".
				Functions::squote($this->gidlivre()).",".
				Functions::squote($this->giduser())
			);

			$this->create($options,$erreur,$idreq); 
			return $idreq;
		}

		public function get($options = array()){

			$resultat = $this->read($options);

			$reservations = array();
			foreach ($resultat as $key => $value) {
				$reservation = new Reservation();
				$reservation->sid($value['id']);
				$reservation->sdatedebut($value['date_debut']);
				$reservation->sdatefin($value['date_fin']);
				$reservation->sidlivre($value['id_livre']);
				$reservation->siduser($value['id_user']);

				array_push($reservations, $reservation);
			}

			return $reservations;
		}

		public function getByUser($options = array()){
			require_once 'user.class.php';
			require_once 'livre.class.php';


			// coje la columna de id user de la tabla de reservation y con esa informacion te da la informacion que contine user y te pega las 2 informaciones en la misma tabla
			$sql = "SELECT reservation.*,user.nom,user.prenom,livre.titre FROM reservation 
					LEFT JOIN user ON reservation.id_user = user.id
					LEFT JOIN livre ON reservation.id_livre = livre.id
					";

			$resultat = $this->custom($sql,true); 

			$reservations = array();
			foreach ($resultat as $key => $value) {

				$reservation = new Reservation();
				$reservation->sid($value['id']);
				$reservation->sdatedebut($value['date_debut']);
				$reservation->sdatefin($value['date_fin']);
				$reservation->sidlivre($value['id_livre']);
				$reservation->siduser($value['id_user']);

				$user = new User();
				$user->snom($value['nom']);
				$user->sprenom($value['prenom']);

				$livre = new livre();
				$livre->stitre($value['titre']);


				array_push($reservations, array($reservation,$user,$livre));
			}

			return $reservations;
		}

		public function up($options = array()){
			$this->update($options);
		}

		public function del($options = array()){
			$this->delete($options);
		}

	}