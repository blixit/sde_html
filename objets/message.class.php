<?php
	class Message extends Modele{
		private $_id = 0;
		private $_date = time();
		private $_nom = '';
		private $_email = '';
		private $_message = '';

		function __construct(){
			$this->_default_tables = 'message';
		}

		// GETTERS & SETTERS

		public function gid(){
			return $this->_id;
		}

		public function sid($value){
			$this->_id = $value;
		}

		public function gdate(){
			return $this->_date;
		}

		public function sdate($value){
			$this->_date = $value;
		}		

		public function gnom(){
			return $this->_nom;
		}

		public function snom($value){
			$this->_nom = $value;
		}

		public function gemail(){
			return $this->_email;
		}

		public function semail($value){
			$this->_email = $value;
		}

		public function gmessage(){
			return $this->_message;
		}

		public function smessage($value){
			$this->_message = $value;
		}

		// CRUD FONCTIONS

		public function add(){
			$erreur;
			$idreq;
			$options = array('values' =>
				Functions::squote($this->gid()).",".
				Functions::squote($this->gdate()).",".
				Functions::squote($this->gnom()).",".
				Functions::squote($this->gemail()).",".
				Functions::squote($this->gmessage())
			);

			$this->create($options,$erreur,$idreq);
		}

		public function get($options = array()){
			$resultat = $this->read($options);

			$messages = array();
			foreach($resultat as $key => $value){
				$message = new Message();
				$message->$sid($value['id']);
				$message->$sdate($value['date']);
				$message->$snom($value['nom']);
				$message->$semail($value['email']);
				$message->$smessage($value['message']);

				array_push($messages,$message);
			}

			return $messages;
		}

		public function up($options = array()){
			$this->update($options);
		}

		public function del($options = array()){
			$this->delete($options);
		}

	}
