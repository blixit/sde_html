<?php
	/**
	*	\class BibliothequeController 
	*	\biref Cette classe fait office de classe mère pour tous les controllers. Elle définit 
	*	des fonctions permettant de traiter les requetes et d'afficher les résultats.
	*	Elle traite en particulier les requetes liées au controller \e BibliothequeController
	*/
	class BibliothequeController extends Controller{ 
		public function __construct(){
			//on appel le constructeur de la Classe Controller
			parent::__construct(); 

			//le nom de la classe actuelle
			$this->_name = 'bibliotheque';
			//on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
		}

		/**
		*	\fn index
		*/
	

		public function chercher(){
			$form=$this->_data;

			// if (!empty($form)) {
			//	$user = $this->loadModele('user');
			//	$resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);

			$this->_view = '/bibliotheque/chercher';
			$this->_title .= ' | CHERCHER';
		}
		public function afficher(){
			$form=$this->_data;

			// if (!empty($form)) {
			//	$user = $this->loadModele('user');
			//	$resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);

			$this->_view = '/bibliotheque/afficher';
			$this->_title .= ' | AFFICHER';
		}
	}