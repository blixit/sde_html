<?php
	/**
	*	\class ReservationController 
	*	\biref Cette classe fait office de classe mère pour tous les controllers. Elle définit 
	*	des fonctions permettant de traiter les requetes et d'afficher les résultats.
	*	Elle traite en particulier les requetes liées au controller \e ReservationController
	*/
	class ReservationController extends Controller{ 
		public function __construct(){
			//on appel le constructeur de la Classe Controller
			parent::__construct(); 

			//le nom de la classe actuelle
			$this->_name = 'reservation';
			//on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
			$this->_connexionNeeded = true;
		}

		public function index()
		{
			$reservation = $this->loadModele('reservation');
			//$resultat = $reservation->get(array("conditions"=>"nom = 'Inurritegui'"));  
			//var_dump($resultat);

			$reservation->sid(15);
			$reservation->sdatedebut(time());
			$reservation->sdatefin(time());
			$reservation->sidlivre(25);
			$reservation->siduser(20);
				
		// con esto esta disponible en vue. Ya puedes poner echo	
			$this->_data['reservation'] = $reservation; 
			$this->_view = '/reservation/index';

		}

		/**
		* Fuction pour faire une réservation
		*	\fn index
		*/
		public function reserve(){
			$form=$this->_data; 
			//include_once $this->PATH_TO_OBJECTS.'/user.class.php'; 
			//user = $this->cast('User',$_SESSION['user']);
			//var_dump($user->gnom()); die;

			if (!empty($form)) {

				$reservation = $this->loadModele('reservation'); 

				$reservation->sid("");
				$reservation->sdatedebut($form["date_debut"]);
				$reservation->sdatefin($form["date_fin"]);
				$reservation->sidlivre($form["id_livre"]);
				$reservation->siduser($_SESSION['user_id']);

				$id = $reservation->add($erreur);
				if(!(is_numeric($id) && $id>0 ) ){
					$this->_data['erreur'][] = $erreur;
				}else{
					$this->_data['notification'][] = "La résérvation s'est bien déroulée.";
				}

			}
			//$this-> ->add()

			$this->_view = '/reservation/reserve';
			$this->_title .= ' | RESERVE';
		}

		/**
		* Fuction pour delete une réservation
		*	\fn index
		*/
	

		public function delete(){ 
			$id = $this->_urlParameters['id'];
 
			$reservation = $this->loadModele('reservation');
			$reservation->delete(array("conditions"=>"id = ".$id));

 			$this->redirect('/?controller=reservation&action=afficher');
 
		}

		public function afficher(){
			$reservation = $this->loadModele('reservation');
			$lesreservations = $reservation->getByUser(); 
			$this->_data['lesreservations'] = $lesreservations;

			$this->_view = '/reservation/afficher';
			$this->_title .= ' | AFFICHER';
		}
	}