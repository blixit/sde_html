<?php
	/* class livreController
	*/
	class LivreController extends Controller{

		public function __construct(){
			parent::__construct();

			// le nom de la classe actuelle
			$this->_name = 'livre';
			// on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
		}

		/**
		*	\fn index
		*/
		public function afficher(){
			$livre = $this->loadModele('livre');
			$leslivres = $livre->get();

			$this->_data['livres'] = $leslivres;
			$this->_view = '/livre';

		}

		public function cherche($options = array()){
			$livre = $this->loadModele('livre');
			$resultat = $livre->get($options);

			$this->_data['livres'] = $resultat;
			$this->_view = '/livre';
		}

		public function enregistrer(){
			$livre = $this->loadModele('livre');
			$livre->stitre();
			$livre->sauteur();
			$livre->sannee();
			$libre->sidgenre();
			$livre->sidcategorie();
			$livre->add();

			$this->_view = '/livre';
		}

		public function miseajour(){
			$this->_view = '/livre';
		}

		public function effacer(){
			$this->_view = '/livre';
		}
	}