<?php
	/**
	*	\class ContactController 
	*	\biref Cette classe fait office de classe mère pour tous les controllers. Elle définit 
	*	des fonctions permettant de traiter les requetes et d'afficher les résultats.
	*	Elle traite en particulier les requetes liées au controller \e ContactController
	*/
	class ContactController extends Controller{ 
		public function __construct(){
			//on appel le constructeur de la Classe Controller
			parent::__construct(); 

			//le nom de la classe actuelle
			$this->_name = 'contact';
			//on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
		}

		/**
		*	\fn index
		*/
/**
	*	\function send
	*	\cette function 
	*/

		public function send(){
			$form=$this->_data;

			// if (!empty($form)) {
			//	$user = $this->loadModele('user');
			//	$resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);

			$this->_view = '/contact/send';
			$this->_title .= ' | SEND';

		}
/**
	*	\function read. 
	*/		

		public function read(){
			$form=$this->_data;

			//if (!empty($form)) {
				// $user = $this->loadModele('user');
				// $resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);
			
			//}

			$this->_view = '/contact/read';
			$this->_title .= '| READ';
		}

		public function delete(){
			$form=$this->_data;

			//if (!empty($form)) {
				// $user = $this->loadModele('user');
				// $resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);
			
			//}

			$this->_view = '/contact/delete';
			$this->_title .= '| DELETE';
		}
	}