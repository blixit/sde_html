<?php
	/**
	*	\class UserController 
	*	\biref Cette classe fait office de classe mère pour tous les controllers. Elle définit 
	*	des fonctions permettant de traiter les requetes et d'afficher les résultats.
	*	Elle traite en particulier les requetes liées au controller \e AccueilController
	*/
	class UserController extends Controller{ 
		public function __construct(){
			//on appel le constructeur de la Classe Controller
			parent::__construct(); 

			//le nom de la classe actuelle
			$this->_name = 'user';
			//on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
		}

		/**
		*	\fn index
		*/
		public function index(){
			// on instancie un objet de la classe User
			$user = $this->loadModele('user');
			$resultat = $user->get(array("conditions"=>"nom = 'Inurritegui'"));  
			var_dump($resultat);

			$user->sid(15);
			$user->snom("CLUB");
			$user->sprenom("SDE");
			$user->slogin("club_sde");
			$user->spass(sha1("123456789o.O"));
			$user->sinscription(time()); //temps actuel en secondes
			$user->add();
			$this->_data['user'] = $user; 

			for ($i=0; $i < 10; $i++) { 
				$this->_data['data'][$i] = $i;
			}

			$this->_view = '/ficheutilisateur'; 
			$this->_title .= ' | CONNEXION'; 
		}

		

		public function connect(){
			$form=$this->_data;

			if (!empty($form)) {
				$user = $this->loadModele('user');
				$resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);
				// Aqui se mira en la base de datos si el login que se quiere conectar existe y si es asi, rellena todos los dato en un OBJETO
			}
			$this->_view = '/user/connect'; 
		}
	}