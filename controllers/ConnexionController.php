<?php
	/**
	*	\class ConnexionController 
	*	\biref Cette classe fait office de classe mère pour tous les controllers. Elle définit 
	*	des fonctions permettant de traiter les requetes et d'afficher les résultats.
	*	Elle traite en particulier les requetes liées au controller \e ConnexionController
	*/
	class ConnexionController extends Controller{ 
		public function __construct(){
			//on appel le constructeur de la Classe Controller
			parent::__construct(); 

			//le nom de la classe actuelle
			$this->_name = 'connexion';
			//on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
		}

		/**
		*	\fn index
		*/
/**
	*	\function connect 
	*	\cette function verifies si le login existe dans la base de donnes et il mis tous le data sur un objet.
	*/

		public function connect(){
			$form=$this->_data;

			if (!empty($form)) {
				$user = $this->loadModele('user');
				$resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				if(count($resultat)>0){
					$_SESSION['user'] = current($resultat); 
					$_SESSION['user_id'] = current($resultat)->gid(); 
					$_SESSION['user_login'] = current($resultat)->glogin(); 
					$this->redirect('/?controller=accueil');
				}
				else{
					$this->_data['erreur'][] = 'Le login et/ou le  mot passe ne sont pas correctes.'; 
				}
			
			}

			

			$this->_view = '/user/connect';
			$this->_title .= ' | CONNEXION';

		}
/**
	*	\function disconnect 
	*	\cette function disconnects l'utilisateur.
	*/		

		public function disconnect(){
			$form=$this->_data;

			session_destroy();

			//if (!empty($form)) {
				// $user = $this->loadModele('user');
				// $resultat = $user->get(array("conditions"=>"login='".$form['login']."'"));
				// var_dump($resultat);
			
			//}

			$this->_view = '/user/disconnect';
			$this->_title .= '| DISCONNEXION';
		}
	}