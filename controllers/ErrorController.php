<?php
	/**
	*	\class ErrorController 
	*	\biref  
	*/
	class ErrorController extends Controller{ 
		public function __construct(){
			//on appel le constructeur de la Classe Controller
			parent::__construct(); 

			//le nom de la classe actuelle
			$this->_name = 'error';
			//on dit que ce controller a besoin de la base de données
			$this->_modeleNeeded = true;
		}

		/**
		*	\fn index
		*/
		public function index(){ 

			$this->_view = 'error/index'; 
		}
 
	}