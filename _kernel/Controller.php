<?php  
	/**
	*	\class Controller 
	*	\biref Cette classe fait office de classe mère pour tous les controllers. Elle définit 
	*	des fonctions permettant de traiter les requetes et d'afficher les résultats.
	*/
	class Controller{
		public $_name = 'controller'; /**< nom du controller */
		public $_action = ''; /*!< l'action courante */
		public $_layout = 'default'; /**< le layout courant */
		public $_view = ''; /**< la vue courante */
		public $_title = 'BIBLIOTHEQUE'; /**< le layout courant */
		public $_menu = 'default'; /**< le menu courant */
		public $_rendered = false; /**< Indique que la vue a été rendue */
		public $_renderedMenu = false; /**< Indique que le menu a été rendu */
		public $_renderedLayout = false; /**< Indique que le layout a été rendu */
		private $_modeleEngine = NULL; /**< Handle de la base de données */
		public $_modeleNeeded = false; /**< Indique que le controller instancié a besoin de la base de donnnées */
		public $_connexionNeeded = false; /**< Indique que le controller instancié a besoin que l'utilisateur se connecte. */
		public $_session = NULL; /**< Facltatif ;  */
		public $_data = array(); /**< Contient les variables postées ($_POST) */
		public $_datafile = array(); /**< Contient les variables de fichiers ($_FILES) */
		public $_urlParameters = array(); /**< Contient les variables d'url ($_GET) */
		public $INTERNETPATH; /**< Chemin : racine du site */
		public $PATH_TO_CAKESDE; /**< Chemin : chemin absolu de la racine du site */
		public $PATH_TO_VIEWS; /**< Chemin : chemin absolu vers les vues */
		public $PATH_TO_CONTROLLERS; /**< Chemin : chemin absolu vers les controllers */
		public $PATH_TO_OBJECTS; /**< Chemin : chemin absolu vers les objets */
		public $PATH_TO_COMPONENTS; /**< Chemin : chemin absolu composants*/

		/*
		*	Variables de SESSION définies dans ce fichier
		*	$_SESSION['redirect_message']
		*/

		/**
		 * \brief       Constructeur 
		 */
		public function Controller(){
			$this->_data = $_POST;			
			$this->_datafile = $_FILES;			
			$this->_urlParameters = $_GET;			
			$this->INTERNETPATH = '/cakeSDE';
			$this->PATH_TO_CAKESDE = dirname(__file__).'/..';
			$this->PATH_TO_VIEWS = $this->PATH_TO_CAKESDE.'/vues'; 
			$this->PATH_TO_CONTROLLERS = $this->PATH_TO_CAKESDE.'/controllers';
			$this->PATH_TO_OBJECTS = $this->PATH_TO_CAKESDE.'/objets';
			$this->PATH_TO_COMPONENTS= $this->PATH_TO_VIEWS.'/composants';  
		}

		/**
		*	\brief Construit un modele et le renvoie.
		*	\param modele le nom de la classe à instancier.
		*	\return L'objet construit ou NULL.
		*/
		public function loadModele($modele){
			$filename = $this->PATH_TO_OBJECTS.'/'.$modele.'.class.php';

			if(file_exists($filename)){  
				include_once $filename;  
				$modelUCFirst = ucfirst($modele); // on met la 1ere lettre en majuscule
				return new $modelUCFirst();
			}
			return NULL;
		}

		/**
		 * 	\brief       Vérifie si l'action est une méthode de l'instance actuelle
		 * 	\details    Ex : 
		 *		$controller = new Controller();
		 *		$bool = $controller->isAction('isAction'); // True
		 *		$bool = $controller->isAction('index'); // False
		 *
		 *		$controller = new AccueilController();
		 *		$bool = $controller->isAction('isAction'); // True
		 *		$bool = $controller->isAction('index'); // True 
		 * \return    Un \e bool qui indique si la méthode 'action' appartient à la classe.
		 */
		public function isAction($action){ 
			return (in_array($action, get_class_methods(get_class($this)))); 
		}

		/**
		 * \brief       Exécute la méthode donnée en paramètre avec ses paramètres.
		 * \details    Si la action est 'callAction', la méthode renvoie true sans l'exécuter
		 *       pour éviter une boucle infinie
		 * \param    action         Nom de la méthode à exécuter
		 * \param    parameters         Paramètres de la méthode à exécuter
		 * \return    Un \e bool qui indique que la méthode a été lancée, exception faite pour la méthode callAction.
		 */
		public function callAction($action, $parameters=array()){
			if($action == "callAction")
				return true;

			if(!$this->isAction($action))
				return false;

			call_user_method_array( $action , $this, $parameters );

			return true;
		}

		/**
		* 	\brief  Affiche le menu s'il n'a pas encore été rendu
		*	@return le statut du rendu sous forme de booléen
		*/
		public function generateMenu(){
			if($this->_renderedMenu)
				return $this->_renderedMenu;

			//on commence la temporisation
			ob_start();

			//on extrait les variables pour les rendres accsibles depuis le fichier contenant le menu
			extract($this->_data);

			//on inclut le fichier
			$filename = $this->PATH_TO_COMPONENTS.'/menu.'.$this->_menu.'.php';
			if(file_exists($filename)){
				include_once $filename;
				$this->_renderedMenu = true; 
			}
			$menu = ob_get_contents();
			ob_end_clean();

			return $menu;
		}

		/**
		* 	\brief Affiche la vue s'elle n'a pas encore été rendue
		*	@return le statut du rendu sous forme de booléen
		*/
		public function generateView(){
			if($this->_rendered)
				return $this->_rendered;

			//on commence la temporisation
			ob_start();
			
			//on extrait les variables pour les rendres accsibles depuis le fichier contenant le menu
			extract($this->_data);

			//on inclut la vue
			$filename = $this->PATH_TO_VIEWS.'/'.$this->_view.'.php';

			if(file_exists($filename)){
				include_once $filename;
				$this->_rendered = true; 
			}
			$vue = ob_get_contents();
			ob_end_clean();

			return $vue;
		}

		/**
		* 	\brief Affiche le layout s'elle n'a pas encore été rendue
		*	@return le statut du rendu sous forme de booléen
		*/
		public function generateLayout(){
			if($this->_renderedLayout)
				return $this->_renderedLayout;

			//on génère le menu
			$content_menu = $this->generateMenu();
			
			//on génère la vue
			$content_vue = $this->generateView();

			//on envoie les headers par défaut 
			header('Content-Type: text/html; charset=UTF-8');

			$filename = $this->PATH_TO_COMPONENTS.'/layout.'.$this->_layout.'.php';
			
			if(file_exists($filename)){
				include_once $filename;
				$this->_renderedLayout = true; 
			}
			return $this->_renderedLayout;
		}

		/**
		*	\brief Redirige vers le lien donné en paramètre.
		*	Ex : 
		*			redirect('/?account/login'); //redirection en interne sur le site
		*			redirect("http://sde.campus-ecn.fr");
		*	\param link le lien de la redirection
		*	\param code code http
		*	\param name status de la requete
		*	
		*/
		public function redirect($link,$code=200,$name='Success'){      
            header("HTTP/1.1 $code $name");

            if($link[0] == '/')
            	header('Location: '.$this->INTERNETPATH.$link);
            else 
            	header('Location: '.$link);
            
            exit;
    	}
    	/**
		 * Class casting
		 *
		 * @param string|object $destination
		 * @param object $sourceObject
		 * @return object
		 */
		function cast($destination, $sourceObject)
		{
		    if (is_string($destination)) {
		        $destination = new $destination();
		    }
		    $sourceReflection = new ReflectionObject($sourceObject);
		    $destinationReflection = new ReflectionObject($destination);
		    $sourceProperties = $sourceReflection->getProperties();
		    foreach ($sourceProperties as $sourceProperty) {
		        $sourceProperty->setAccessible(true);
		        $name = $sourceProperty->getName();
		        $value = $sourceProperty->getValue($sourceObject);
		        if ($destinationReflection->hasProperty($name)) {
		            $propDest = $destinationReflection->getProperty($name);
		            $propDest->setAccessible(true);
		            $propDest->setValue($destination,$value);
		        } else {
		            $destination->$name = $value;
		        }
		    }
		    return $destination;
		}
	};
	