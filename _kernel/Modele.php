<?php 

	//On a besoin des constantes relatives à la bdd
	require_once 'Constantes.php';
	
	/**
	*	\interface interface_CRUD 
	*	\biref Cette définit les fonctions obligatoires permettant de faire des tranactions avec
	*	le système de stockage : ici la base de données.
	*/
	interface interface_CRUD{
		/**
		*	\brief Ajoute l'objet dans la table
		*/
		public function create($options = array(), &$erreur, &$idreq);

		/**
		*	\brief  Lit n éléments dans la table
		*/
		public function read($options = array('limit' => '0,1'));

		/**
		*	\brief Met à jour l'objet courant.  
		*/
		public function update($options = array());

		/**
		*	\brief Supprime l'élément. Si All=true, on supprime tout. 
		*/
		public function delete($options = array());
	}
	
	/**
	*	\class Modele 
	*	\biref Cette classe contient des fonctions privées que ses filles peuvent utiliser
	*	pour effectuer des transactions avec le système de stockage.
	*/
	class Modele   implements interface_CRUD {   
		private $_host = '';
		private $_dbname = '';
		private $_user = '';
		private $_pass = '';

		/*Ces variables se transmettent par héritage.*/ 

		protected $_default_tables = '*'; /**< _default_tables permettra à chaque objet de définir sa table par défaut*/
		static protected $db = NULL; /**< Le handle qui renvoie l'objet PDO */
		protected $requete; /**< Permet de retrouver la requete */

 		function __construct($h='', $db='', $u='', $p='', &$erreur=''){
 			$this->_host = !empty($h) ? $h : constant('db_host'); 
 			$this->_dbname = !empty($db) ? $db : constant('db_name'); 
 			//si aucun utilisateur n'est donné, on utilise l'utilisateur par défaut
 			if(empty($u)){
 				$this->_user = constant('db_user'); 
	 			$this->_pass = constant('db_pass'); 
 			}else{
 				$this->_user = $u; 
	 			$this->_pass = $p; 
 			} 
	 			
 			//cette partie est volontairement mise dans un bloc if, pour permettre de faire autre chose dans le constructeur
 			//par ex, si un enfant de cette classe veut appeler ce constructeur, il ne faut pas qu'il relance une connexioin déjà active
 			if(self::$db == NULL){ 
 				try {
 					$pdo = new PDO('mysql:host='.$this->_host.';dbname='.$this->_dbname.'',
                                        $this->_user,
                                        $this->_pass,
                                        array(
                                                PDO::ATTR_PERSISTENT => true,
                                                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                                                )
                        );
                    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
                    self::$db = $pdo; 
 				} catch (PDOException $e) {
 					$erreur =  $e->getMessage();   
                    return NULL;
 				}
 			}
 			//autres traitements par exemple
 		}

 		public function getRequete(){
 			return $this->requete;
 		}

 		//Les fonctions CRUD

 		/**
 		*	\brief Ajoute un objet dans la bdd
 		*	\param $options Un tableau (hashtable) contenant les données requises : 
 		*	values, table
 		*/
		public function create($options=array(), &$erreur, &$idreq){
			$sql = 'INSERT INTO ';
			if(isset($options['table'])){
				$sql .= $options['table'];
			}
			else{
				$sql .= $this->_default_tables;
			}
			$sql .= ' VALUES ( ';
			if(isset($options['values'])){
				$sql .= $options['values'];
			}
			else{
				$erreur = 'Sans valeurs';
				return false;
			}
			$sql .= ' );';

            //on sauvegarde la requete
            $this->requete = $sql;  
            
            //on prépare et on excéute la requete
            $q = self::$db->prepare($sql);
            try{
                $q->execute();
                $idreq = self::$db->lastInsertId();
            }
            catch(PDOException $e){ 
            	$erreur = $e->getMessage();
            }
			//libère la connexion pour d'autres requetes
			$q->closeCursor();
		}

 		/**
 		*	\brief Récupère un objet ou plusieurs dans la bdd
 		*	\param $options Un tableau (hashtable) contenant les données requises : 
 		*	values, table
 		*/
		public function read($options=array()){
			$sql = 'SELECT ';

            if(isset($options['champs']))
                $sql .= $options['champs'];
            else
                $sql .= ' DISTINCT * ';

            if(isset($options['tables']))
                $sql .= ' FROM '. $options['tables'];
            else
                $sql .= ' FROM '. $this->_default_tables;  

            // construction de la condition

            if(isset($options['conditions'])){
                $sql .= ' WHERE '; 
                $sql .= $options['conditions']; 
            }

            //ordre des résultats
            if(isset($options['order'])){
                $sql .= ' ORDER BY ';
                $sql .= $options['order'];
            }

            //nombre de résultat  et offset
            if(isset($options['limit'])){
                $sql .= ' LIMIT ';
                $sql .= $options['limit'];
            }

            //on sauvegarde la requete
            $this->requete = $sql;  
            //on prépare et on excéute la requete
            $q = self::$db->prepare($sql);
            try{
                $q->execute();
            }
            catch(PDOException $e){ 
            	$erreur = $e->getMessage();
            }

            //méthode de récupération des données
            if(!empty($options['fecthMethod']))
                $resultat = $q->fetchAll($options['fecthMethod']);
			else	
				$resultat = $q->fetchAll(PDO::FETCH_ASSOC);

			//libère la connexion pour d'autres requetes
			$q->closeCursor();

            return $resultat;
		}

 		/**
 		*	\brief Update un objet ou plusieurs dans la bdd
 		*	\param $options Un tableau (hashtable) contenant les données requises : 
 		*	values, table
 		*/
		public function update($options=array()){

		}

 		/**
 		*	\brief Supprime un objet ou plusieurs dans la bdd
 		*	\param $options Un tableau (hashtable) contenant les données requises : 
 		*	values, table
 		*/
		public function delete($options=array()){
			$sql = 'DELETE ';

            if(isset($options['tables']))
               $sql .= ' FROM '. $options['tables'];
            else
                $sql .= ' FROM '. $this->_default_tables;  

            // construction de la condition
            if(isset($options['conditions'])){
                $sql .= ' WHERE '; 
                $sql .= $options['conditions']; 
            } 

            //on sauvegarde la requete
            $this->requete = $sql;  
            //on prépare et on excéute la requete
            $q = self::$db->prepare($sql);
            try{
                $resultat = $q->execute();
            }
            catch(PDOException $e){ 
            	$erreur = $e->getMessage();
            }
 
			//libère la connexion pour d'autres requetes
			$q->closeCursor();

            return $resultat;
		} 

		/**
 		*	\brief cette function execute la requette donnée en parametres
 		*/
		public function custom($sql,$fetchbool){

            //on sauvegarde la requete
            $this->requete = $sql;  
            //on prépare et on excéute la requete
            $q = self::$db->prepare($sql);
            try{
                $resultat = $q->execute();
            }
            catch(PDOException $e){ 
            	$erreur = $e->getMessage();
            }

            //méthode de récupération des données
            if($fetchbool == true){
	            if(!empty($options['fecthMethod']))
	                $resultat = $q->fetchAll($options['fecthMethod']);
				else	
					$resultat = $q->fetchAll(PDO::FETCH_ASSOC);
 			}
			//libère la connexion pour d'autres requetes
			$q->closeCursor();

            return $resultat;
		}

	}
