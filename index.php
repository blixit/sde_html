<?php
	/**
	* \file          index.php
	* \author    	Alain 
	* \version   1.0
	* \date       12 Janvier 2016
	* \brief       Fichier de traitement des requetes
	* 
	*/
	session_start();
	// Rapporte les erreurs d'exécution de script
	error_reporting(E_ERROR | E_WARNING | E_PARSE ); //
	ini_set('error_reporting', E_ALL); 
	ini_set("display_errors", "1");


	//Traitement des paramètres
	//controller et action par défaut
	$default_controller = 'Accueil';
	$default_action = 'index';

	//controller et action demandé par l'utilisateur
	$controllerName = !empty($_GET['controller']) ? ucfirst($_GET['controller'])  : $default_controller;
	$action = !empty($_GET['action']) ? $_GET['action'] : $default_action;

	//Chargement du coeur de l'application
	if(!file_exists('_kernel/Controller.php')){
		echo "Le moteur de l'application est manquant.";
		return;
	}

	//module du kernel
	include_once('_kernel/Constantes.php');
	include_once('_kernel/Controller.php');
	include_once('_kernel/Modele.php');  
	include_once('_kernel/fonctions.php');

	//Chargement du controller
	$controllerName .= 'Controller'; 
	$controllerFile = $PATH_TO_CONTROLLERS.'/'.$controllerName.'.php';
	if(!file_exists($controllerFile)){
		$message = "Le controlleur demandé ne répond pas ou n'existe plus. Cette requête a été rejeté.";
		echo $message;
		$controller->redirect('/?error');
		return;
	}

	include_once($controllerFile);

	//instanciation du controller (c'est ici qu'on crée l'objet controller. par Ex: new AccueilController(); )
	$controller = new $controllerName();

	if(empty(get_class($controller))){
		$message = "Une erreur est survenue lors de l'instanciation du controller.";
		$controller->redirect('/?error');
		return;
	}

	//Chargement du SGBD (système de Gestion de Base de Dponnées)
	if($controller->_modeleNeeded){
		$controller->_modeleEngine = new Modele('127.0.0.1','bibliotheque','root','',$erreur);
		//Si la bdd est requise dans la suite, toute erreur de connection doit arrêter le script
		if($controller->_modeleEngine == NULL || !empty($erreur)){
			die($erreur); // die de test
			//controller::redirect('error.php');//
		}
	} 

	//Verification de la connexion
	if($controller->_connexionNeeded){  
		if(empty($_SESSION['user'])){  
			$controller->redirect('/?controller=connexion&action=connect');
		}
	}

	//Appel de la fonction demandée 
	if(!$controller->callAction($action)){
		$controller->redirect('/?error');
		return;
	}

	//Génération de la vue
	$controller->generateLayout();

 
 
